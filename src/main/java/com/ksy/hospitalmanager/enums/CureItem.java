package com.ksy.hospitalmanager.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum CureItem {
    HERBAL_MEDICINE("한약", 25000, 50000),
    ACUPUNCTURE("약침", 3000, 20000),
    CHUNA("추나요법", 70000, 70000)
    ;

    private final String cureName;
    private final double insurancePrice;
    private final double uninsuredPrice;
}
