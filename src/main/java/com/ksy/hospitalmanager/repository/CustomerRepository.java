package com.ksy.hospitalmanager.repository;

import com.ksy.hospitalmanager.entity.Customer;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface CustomerRepository extends JpaRepository<Customer, Long> {
    List<Customer> findAllByCustomerNameOrderByCustomerNameDesc(String customerName);
}
