package com.ksy.hospitalmanager.repository;

import com.ksy.hospitalmanager.entity.CureHistory;
import org.springframework.data.jpa.repository.JpaRepository;

import java.time.LocalDate;
import java.util.List;

public interface CureHistoryRepository extends JpaRepository<CureHistory, Long> {
    List<CureHistory> findAllByDateCureOrderByIdDesc(LocalDate dateCure);
    List<CureHistory> findAllByIsInsuranceAndIsCalculationAndDateCureOrderByIdDesc(boolean isInsurance, boolean isCalculation, LocalDate dateCure);
    List<CureHistory> findAllByDateCureBetweenAndIsCalculationAndIsInsuranceOrderByIdDesc(LocalDate start, LocalDate end, boolean isCalculation, boolean isInsurance);
}
