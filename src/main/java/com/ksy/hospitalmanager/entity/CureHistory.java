package com.ksy.hospitalmanager.entity;

import com.ksy.hospitalmanager.enums.CureItem;
import com.ksy.hospitalmanager.interfaces.CommonModelBuilder;
import com.ksy.hospitalmanager.model.CureHistoryRequest;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalTime;

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class CureHistory {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "customerId", nullable = false)
    private Customer customer;

    @Column(nullable = false, length = 20)
    @Enumerated(value = EnumType.STRING)
    private CureItem cureItem;

    @Column(nullable = false)
    private Boolean isInsurance;

    @Column(nullable = false)
    private Double price;

    @Column(nullable = false)
    private LocalDate dateCure;

    @Column(nullable = false)
    private LocalTime timeCure;

    @Column(nullable = false)
    private Boolean isCalculation;

    public void putCalculationCompleted() {
        this.isCalculation = true;
    }

    private CureHistory(CureHistoryBuilder builder) {
        this.customer = builder.customer;
        this.cureItem = builder.cureItem;
        this.isInsurance = builder.isInsurance;
        this.price = builder.price;
        this.dateCure = builder.dateCure;
        this.timeCure = builder.timeCure;
        this.isCalculation = builder.isCalculation;
    }

    public static class CureHistoryBuilder implements CommonModelBuilder<CureHistory> {
        private final Customer customer;
        private final CureItem cureItem;
        private final Boolean isInsurance;
        private final Double price;
        private final LocalDate dateCure;
        private final LocalTime timeCure;
        private final Boolean isCalculation;

        public CureHistoryBuilder(Customer customer, CureHistoryRequest request) {
            this.customer = customer;
            this.cureItem = request.getCureItem();
            this.isInsurance = request.getIsInsurance();
            this.price = request.getIsInsurance() ? request.getCureItem().getInsurancePrice() : request.getCureItem().getUninsuredPrice();
            this.dateCure = LocalDate.now();
            this.timeCure = LocalTime.now();
            this.isCalculation = false;
        }

        @Override
        public CureHistory build() {
            return new CureHistory(this);
        }
    }
}
