package com.ksy.hospitalmanager.entity;

import com.ksy.hospitalmanager.interfaces.CommonModelBuilder;
import com.ksy.hospitalmanager.model.CustomerInfoUpdateRequest;
import com.ksy.hospitalmanager.model.CustomerRequest;
import com.ksy.hospitalmanager.model.MemoUpdateRequest;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class Customer {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false, length = 20)
    private String customerName;

    @Column(nullable = false, length = 14)
    private String registrationNumber;

    @Column(nullable = false, length = 13)
    private String customerPhone;

    @Column(nullable = false, length = 50)
    private String customerAddress;

    @Column(nullable = false, columnDefinition = "TEXT")
    private String purposeVisit;

    @Column(nullable = false)
    private LocalDate dateVisit;

    @Column(length = 20)
    private String memo;

    public void putInfo(CustomerInfoUpdateRequest request) {
        this.customerName = request.getCustomerName();
        this.customerPhone = request.getCustomerPhone();
    }

    public void putMemo(MemoUpdateRequest request) {
        this.memo = request.getMemo();
    }

    // 왜 프라이빗인가... 그야... 바깥에서 접근 못하게 하려고 Noar 달았는데 퍼블릭하면 의미가 없잖아
    // 빌더 만든 다음 빌더 타입 가져오기 빌더에 값 입력되면 실행~ 빌더를 통해서만 값을 채운다
    // 여기 this는 Customer. 내가 있는 이 위치
    // 좀 복잡시럽지만 빌더를 통해서 먼저 값을 받고 얘가 빌더에서 값을 하나 더 받고...
    private Customer(CustomerBuilder builder) {
        this.customerName = builder.customerName;
        this.registrationNumber = builder.registrationNumber;
        this.customerPhone = builder.customerPhone;
        this.customerAddress = builder.customerAddress;
        this.purposeVisit = builder.purposeVisit;
        this.dateVisit = builder.dateVisit;
    }

    // 부모 클래스 이름 + 빌더
    // 빌더 패턴 쓸 클래스니까 implements 빌더<엔티티>
    public static class CustomerBuilder implements CommonModelBuilder<Customer> {
        private final String customerName;
        private final String registrationNumber;
        private final String customerPhone;
        private final String customerAddress;
        private final String purposeVisit;
        private final LocalDate dateVisit;

        // nullable = false가 아니면 넣지 말기 필수가 아닌 건 U로 뺀다
        // 위에는 클래스고 아래는 생성자? 일단 빌더 안에 자리가 있어야 뭘 넣을 수 있으니까? 적어두는? 건가?
        // 기존 세터는 바로 엔티티에 넣어서 자리가 마련되어 있었음... 근데 빌더는 비었으니까?

        // 빌더는 new~ 가 가능해야 하니까 만들기. 이름은 위와 똑같이 해야 한다 그게 생성자니까...
        public CustomerBuilder(CustomerRequest request) {
            this.customerName = request.getCustomerName();
            this.registrationNumber = request.getRegistrationNumber();
            this.customerPhone = request.getCustomerPhone();
            this.customerAddress = request.getCustomerAddress();
            this.purposeVisit = request.getPurposeVisit();
            this.dateVisit = LocalDate.now();
        }

        // new는 여기서 할 수 있다. 위에 <Customer>가 들어있어서 자동으로 반환되는 타입이 Customer
        // private Customer(CustomerBuilder builder)는 빌더를 넣어줘야 한다
        // 여기 자체가 빌더니까 this 넣으면 됨
        // 서비스에서 접근해야 하니까 얜 public. 근데 Customer에 빌더값을 실질적으로 옮겨넣어주는 애는 private Customer()
        // 엔티티에 값을 입력하는 건 엔티티()가 할 일이니까? 기존에 public Customer()가 있었던 것처럼?
        @Override
        public Customer build() {
            return new Customer(this);
        }
    }
}
