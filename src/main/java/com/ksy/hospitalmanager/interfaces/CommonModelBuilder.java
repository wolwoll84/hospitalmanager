package com.ksy.hospitalmanager.interfaces;

public interface CommonModelBuilder<T> {
    T build();
}
