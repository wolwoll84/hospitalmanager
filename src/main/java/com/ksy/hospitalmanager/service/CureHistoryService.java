package com.ksy.hospitalmanager.service;

import com.ksy.hospitalmanager.entity.CureHistory;
import com.ksy.hospitalmanager.entity.Customer;
import com.ksy.hospitalmanager.model.CureHistoryRequest;
import com.ksy.hospitalmanager.model.HistoryInsuranceCorporationItem;
import com.ksy.hospitalmanager.model.HistoryItem;
import com.ksy.hospitalmanager.model.HistoryResponse;
import com.ksy.hospitalmanager.repository.CureHistoryRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class CureHistoryService {
    private final CureHistoryRepository cureHistoryRepository;

    public void setHistory(Customer customer, CureHistoryRequest request) {
        CureHistory addData = new CureHistory.CureHistoryBuilder(customer, request).build();

        cureHistoryRepository.save(addData);
    }

    public List<HistoryItem> getHistoriesByDate(LocalDate searchDate) {
        List<HistoryItem> result = new LinkedList<>();

        List<CureHistory> originData = cureHistoryRepository.findAllByDateCureOrderByIdDesc(searchDate);

        originData.forEach(item -> result.add(new HistoryItem.HistoryItemBuilder(item).build()));

        return result;
    }

    public List<HistoryInsuranceCorporationItem> getHistoriesByInsurance(LocalDate searchDate) {
        List<HistoryInsuranceCorporationItem> result = new LinkedList<>();

        List<CureHistory> originData = cureHistoryRepository.findAllByIsInsuranceAndIsCalculationAndDateCureOrderByIdDesc(true, true, searchDate);

        originData.forEach(item -> result.add(new HistoryInsuranceCorporationItem.HistoryInsuranceCorporationItemBuilder(item).build()));

        return result;
    }

    public List<HistoryInsuranceCorporationItem> getMonthInsurance(LocalDate start, LocalDate end) {
        List<HistoryInsuranceCorporationItem> result = new LinkedList<>();

        List<CureHistory> originData = cureHistoryRepository.findAllByDateCureBetweenAndIsCalculationAndIsInsuranceOrderByIdDesc(start, end, true, true);

        originData.forEach(item -> result.add(new HistoryInsuranceCorporationItem.HistoryInsuranceCorporationItemBuilder(item).build()));

        return result;
    }

    public HistoryResponse getHistory(long historyId) {
        CureHistory originData = cureHistoryRepository.findById(historyId).orElseThrow();

        return new HistoryResponse.HistoryResponseBuilder(originData).build();
    }

    public void putCalculationCompleted(long id) {
        CureHistory originData = cureHistoryRepository.findById(id).orElseThrow();
        originData.putCalculationCompleted();

        cureHistoryRepository.save(originData);
    }
}
