package com.ksy.hospitalmanager.service;

import com.ksy.hospitalmanager.entity.Customer;
import com.ksy.hospitalmanager.model.CustomerInfoUpdateRequest;
import com.ksy.hospitalmanager.model.CustomerRequest;
import com.ksy.hospitalmanager.model.MemoUpdateRequest;
import com.ksy.hospitalmanager.repository.CustomerRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class CustomerService {
    private final CustomerRepository customerRepository;

    // 여기서 불러오는 CustomerBuilder는 아래쪽
    // 근데 엔티티에 저장하려면 Customer 모양이어야 하니까 public Customer build() 이용해서 엔티티 모양으로 바꾸기
    public void setCustomer(CustomerRequest request) {
        Customer addData = new Customer.CustomerBuilder(request).build();

        customerRepository.save(addData);
    }

    public Customer getCustomer(long customerId) {
        return customerRepository.findById(customerId).orElseThrow();
    }

    public List<Customer> getCustomerNameSearch(String customerName) {
        return customerRepository.findAllByCustomerNameOrderByCustomerNameDesc(customerName);
    }

    public void putInfo(long id, CustomerInfoUpdateRequest request) {
        Customer addData = customerRepository.findById(id).orElseThrow();
        addData.putInfo(request);

        customerRepository.save(addData);
    }

    public void putMemo(long id, MemoUpdateRequest request) {
        Customer originData = customerRepository.findById(id).orElseThrow();
        originData.putMemo(request);

        customerRepository.save(originData);
    }
}
