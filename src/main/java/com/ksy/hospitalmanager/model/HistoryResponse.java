package com.ksy.hospitalmanager.model;

import com.ksy.hospitalmanager.entity.CureHistory;
import com.ksy.hospitalmanager.enums.CureItem;
import com.ksy.hospitalmanager.interfaces.CommonModelBuilder;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDate;
import java.time.LocalTime;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class HistoryResponse {
    private Long customerId;
    private String customerName;
    private String registrationNumber;
    private String customerPhone;
    private String memo;
    private String cureItemName;
    private String isInsurance;
    private Double price;
    private LocalDate dateCure;
    private LocalTime timeCure;
    private String isCalculation;

    private HistoryResponse(HistoryResponseBuilder builder) {
        this.customerId = builder.customerId;
        this.customerName = builder.customerName;
        this.registrationNumber = builder.registrationNumber;
        this.customerPhone = builder.customerPhone;
        this.memo = builder.memo;
        this.cureItemName = builder.cureItemName;
        this.isInsurance = builder.isInsurance;
        this.price = builder.price;
        this.dateCure = builder.dateCure;
        this.timeCure = builder.timeCure;
        this.isCalculation = builder.isCalculation;
    }

    public static class HistoryResponseBuilder implements CommonModelBuilder<HistoryResponse> {
        private final Long customerId;
        private final String customerName;
        private final String registrationNumber;
        private final String customerPhone;
        private final String memo;
        private final String cureItemName;
        private final String isInsurance;
        private final Double price;
        private final LocalDate dateCure;
        private final LocalTime timeCure;
        private final String isCalculation;

        public HistoryResponseBuilder(CureHistory cureHistory) {
            this.customerId = cureHistory.getCustomer().getId();
            this.customerName = cureHistory.getCustomer().getCustomerName();
            this.registrationNumber = cureHistory.getCustomer().getRegistrationNumber();
            this.customerPhone = cureHistory.getCustomer().getCustomerPhone();
            this.memo = cureHistory.getCustomer().getMemo();
            this.cureItemName = cureHistory.getCureItem().getCureName();
            this.isInsurance = cureHistory.getIsInsurance() ? "보험 대상" : "비보험";
            this.price = cureHistory.getPrice();
            this.dateCure = cureHistory.getDateCure();
            this.timeCure = cureHistory.getTimeCure();
            this.isCalculation = cureHistory.getIsCalculation() ? "정산" : "미정산";
        }

        @Override
        public HistoryResponse build() {
            return new HistoryResponse(this);
        }
    }
}
