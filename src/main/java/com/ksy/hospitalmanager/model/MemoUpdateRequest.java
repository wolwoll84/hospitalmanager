package com.ksy.hospitalmanager.model;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

@Getter
@Setter
public class MemoUpdateRequest {
    @Length(min = 1, max = 20)
    private String memo;
}
