package com.ksy.hospitalmanager.model;

import com.ksy.hospitalmanager.enums.CureItem;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.validation.constraints.NotNull;

@Getter
@Setter
public class CureHistoryRequest {
    @NotNull
    @Enumerated(value = EnumType.STRING)
    private CureItem cureItem;

    @NotNull
    private Boolean isInsurance;
}
