package com.ksy.hospitalmanager.model;

import com.ksy.hospitalmanager.entity.CureHistory;
import com.ksy.hospitalmanager.interfaces.CommonModelBuilder;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.time.LocalDate;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class HistoryInsuranceCorporationItem {
    private String customerName;
    private String registrationNumber;
    private String customerPhone;
    private String cureItemName;
    private Double primeCost;
    private Double customerContribution;
    private Double billingAmount;
    private LocalDate dateCure;

    private HistoryInsuranceCorporationItem(HistoryInsuranceCorporationItemBuilder builder) {
        this.customerName = builder.customerName;
        this.registrationNumber = builder.registrationNumber;
        this.customerPhone = builder.customerPhone;
        this.cureItemName = builder.cureItemName;
        this.primeCost = builder.primeCost;
        this.customerContribution = builder.customerContribution;
        this.billingAmount = builder.billingAmount;
        this.dateCure = builder.dateCure;
    }

    public static class HistoryInsuranceCorporationItemBuilder implements CommonModelBuilder<HistoryInsuranceCorporationItem> {
        private final String customerName;
        private final String registrationNumber;
        private final String customerPhone;
        private final String cureItemName;
        private final Double primeCost;
        private final Double customerContribution;
        private final Double billingAmount;
        private final LocalDate dateCure;

        public HistoryInsuranceCorporationItemBuilder(CureHistory cureHistory) {
            this.customerName = cureHistory.getCustomer().getCustomerName();
            this.registrationNumber = cureHistory.getCustomer().getRegistrationNumber();
            this.customerPhone = cureHistory.getCustomer().getCustomerPhone();
            this.cureItemName = cureHistory.getCureItem().getCureName();
            this.primeCost = cureHistory.getCureItem().getUninsuredPrice();
            this.customerContribution = cureHistory.getPrice();
            this.billingAmount = cureHistory.getCureItem().getUninsuredPrice() - cureHistory.getPrice();
            this.dateCure = cureHistory.getDateCure();
        }

        @Override
        public HistoryInsuranceCorporationItem build() {
            return new HistoryInsuranceCorporationItem(this);
        }
    }
}
