package com.ksy.hospitalmanager.model;

import com.ksy.hospitalmanager.entity.CureHistory;
import com.ksy.hospitalmanager.interfaces.CommonModelBuilder;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.time.LocalDate;
import java.time.LocalTime;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class HistoryItem {
    private Long historyId;
    private Long customerId;
    private String customerName;
    private String registrationNumber;
    private String customerPhone;
    private String cureItem;
    private String isInsurance;
    private Double price;
    private LocalDate dateCure;
    private LocalTime timeCure;
    private String isCalculation;

    private HistoryItem(HistoryItemBuilder builder) {
        this.historyId = builder.historyId;
        this.customerId = builder.customerId;
        this.customerName = builder.customerName;
        this.registrationNumber = builder.registrationNumber;
        this.customerPhone = builder.customerPhone;
        this.cureItem = builder.cureItem;
        this.isInsurance = builder.isInsurance;
        this.price = builder.price;
        this.dateCure = builder.dateCure;
        this.timeCure = builder.timeCure;
        this.isCalculation = builder.isCalculation;
    }

    public static class HistoryItemBuilder implements CommonModelBuilder<HistoryItem> {
        private final Long historyId;
        private final Long customerId;
        private final String customerName;
        private final String registrationNumber;
        private final String customerPhone;
        private final String cureItem;
        private final String isInsurance;
        private final Double price;
        private final LocalDate dateCure;
        private final LocalTime timeCure;
        private final String isCalculation;

        public HistoryItemBuilder(CureHistory cureHistory) {
            this.historyId = cureHistory.getId();
            this.customerId = cureHistory.getCustomer().getId();
            this.customerName = cureHistory.getCustomer().getCustomerName();
            this.registrationNumber = cureHistory.getCustomer().getRegistrationNumber();
            this.customerPhone = cureHistory.getCustomer().getCustomerPhone();
            this.cureItem = cureHistory.getCureItem().getCureName();
            this.isInsurance = cureHistory.getIsInsurance() ? "예" : "아니오";
            this.price = cureHistory.getPrice();
            this.dateCure = cureHistory.getDateCure();
            this.timeCure = cureHistory.getTimeCure();
            this.isCalculation = cureHistory.getIsCalculation() ? "예" : "아니오";
        }

        @Override
        public HistoryItem build() {
            return new HistoryItem(this);
        }
    }
}
