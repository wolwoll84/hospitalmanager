package com.ksy.hospitalmanager.controller;

import com.ksy.hospitalmanager.entity.Customer;
import com.ksy.hospitalmanager.model.CustomerInfoUpdateRequest;
import com.ksy.hospitalmanager.model.CustomerRequest;
import com.ksy.hospitalmanager.model.MemoUpdateRequest;
import com.ksy.hospitalmanager.service.CustomerService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/customer")
public class CustomerController {
    private final CustomerService customerService;

    @PostMapping("/new")
    public String setCustomer(@RequestBody @Valid CustomerRequest request) {
        customerService.setCustomer(request);

        return "등록되었습니다.";
    }

    @GetMapping("/search")
    public List<Customer> getCustomerNameSearch(@RequestParam("customerName") String customerName) {
        return customerService.getCustomerNameSearch(customerName);
    }

    @PutMapping("/info/customer-id/{customerId}")
    public String putInfo(@PathVariable long customerId, @RequestBody @Valid CustomerInfoUpdateRequest request) {
        customerService.putInfo(customerId, request);

        return "개인정보가 수정되었습니다.";
    }

    @PutMapping("/memo/cutomer-id/{customerId}")
    public String putMemo(@PathVariable long customerId, @RequestBody @Valid MemoUpdateRequest request) {
        customerService.putMemo(customerId, request);

        return "메모 등록";
    }
}
