package com.ksy.hospitalmanager.controller;

import com.ksy.hospitalmanager.entity.Customer;
import com.ksy.hospitalmanager.model.CureHistoryRequest;
import com.ksy.hospitalmanager.model.HistoryInsuranceCorporationItem;
import com.ksy.hospitalmanager.model.HistoryItem;
import com.ksy.hospitalmanager.model.HistoryResponse;
import com.ksy.hospitalmanager.service.CureHistoryService;
import com.ksy.hospitalmanager.service.CustomerService;
import lombok.RequiredArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.time.LocalDate;
import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/history")
public class CureHistoryController {
    private final CureHistoryService cureHistoryService;
    private final CustomerService customerService;

    @PostMapping("/new/customer-id/{customerId}")
    public String setHistory(@PathVariable long customerId, @RequestBody @Valid CureHistoryRequest request) {
        Customer customer = customerService.getCustomer(customerId);
        cureHistoryService.setHistory(customer, request);

        return "추가됐습니다.";
    }

    @GetMapping("/all/date")
    public List<HistoryItem> getHistoriesByDate(@RequestParam("searchDate") @DateTimeFormat(pattern = "yyyy-MM-dd") LocalDate searchDate) {
        return cureHistoryService.getHistoriesByDate(searchDate);
    }

    @GetMapping("/all/insurance")
    public List<HistoryInsuranceCorporationItem> getHistoriesByInsurance(@RequestParam("searchDate") @DateTimeFormat(pattern = "yyyy-MM-dd") LocalDate searchDate) {
        return cureHistoryService.getHistoriesByInsurance(searchDate);
    }

    @GetMapping("/all/insurance/month")
    public List<HistoryInsuranceCorporationItem> getMonthInsurance(
            @RequestParam("start") @DateTimeFormat(pattern = "yyyy-MM-dd") LocalDate start,
            @RequestParam("end") @DateTimeFormat(pattern = "yyyy-MM-dd") LocalDate end
    ) {
        return cureHistoryService.getMonthInsurance(start, end);
    }

    @GetMapping("/detail/history-id/{historyId}")
    public HistoryResponse getHistory(@PathVariable long historyId) {
        return cureHistoryService.getHistory(historyId);
    }

    @PutMapping("/pay/id/{id}")
    public String putCalculationCompleted(@PathVariable long id) {
        cureHistoryService.putCalculationCompleted(id);

        return "정산되었습니다.";
    }
}
